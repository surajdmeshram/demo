﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication17.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            TempData["TempModel"] = "This is temp data example";
            TempData["TempModelKeep"] = "This is temp data keep example";
            return View();

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}