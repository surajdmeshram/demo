﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication17.Models;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace WebApplication17.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        //[ActionName("A")]
        public ActionResult Index()
        {
            MyDBContext mdb = new MyDBContext();
            ViewBag.StudentList = mdb.Students.ToList();
            //SqlConnection scon = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            //SqlDataAdapter sda = new SqlDataAdapter("StudentById", scon);
            //sda.SelectCommand.CommandType = CommandType.StoredProcedure;
            //DataSet ds = new DataSet();
            //sda.Fill(ds);

            List<Student> s = new List<Student>();
           
            return View("Index");
        }

        public ActionResult Create()
        {
            MyDBContext mdb = new MyDBContext();
            var a = mdb.Students.ToList();
            return View();
        }

        [HttpPost]
        public ActionResult Create(Student student)
        {
            SqlConnection scon = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("StudentInsert", scon);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@StudentName", student.StudentName);
            scon.Open();
            cmd.ExecuteNonQuery();
            scon.Close();
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int StudentId)
        {
            SqlConnection scon = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlDataAdapter sda = new SqlDataAdapter("StudentById", scon);
            sda.SelectCommand.CommandType = CommandType.StoredProcedure;
            sda.SelectCommand.Parameters.AddWithValue("@StudentId", StudentId);
            DataSet ds = new DataSet();
            sda.Fill(ds);
           DataRow dr= ds.Tables[0].Rows[0];
            Student student = new Student();
            student.StudentId = Convert.ToInt32(dr["StudentId"]);
            student.StudentName = dr["StudentName"].ToString();
            return View(student);
        }

        [HttpPost]
        public ActionResult Edit(Student student)
        {
            SqlConnection scon = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("StudentUpdate", scon);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@StudentId", student.StudentId);
            cmd.Parameters.AddWithValue("@StudentName", student.StudentName);
            scon.Open();
            cmd.ExecuteNonQuery();
            scon.Close();
            return RedirectToAction("Index");
        }
    }
}