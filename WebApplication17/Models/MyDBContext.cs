﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication17.Models
{
    public class MyDBContext:DbContext
    {
        public MyDBContext():base("DefaultConnection")
        {

        }

        public DbSet<Student> Students { get; set; }
    }
}