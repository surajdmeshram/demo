﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication17.Models
{
    public class Student
    {
        public int StudentId { get; set; }

        public string StudentName { get; set; }
    }
}